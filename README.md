Развертывания k8s кластера(из отдельных инстансов) в облаке mail cloud, как bare metal с  мониторингом(grafana,loki,prometheus,promtail,node exporter) работающих через ingress

1) склонируйте проект

git clone git@gitlab.com:diplomk8s/app/app.git

или

git clone  https://gitlab.com/diplomk8s/app/app.git

2) добавьте credentials в variables проекта

3) зарегистрируйте shell runner по инструкции https://docs.gitlab.com/runner/install/

4) внесите ip адреса в файл hosts

5) запустите pipeline


для локального запуска используйте команду

ansible-playbook main.yml
